<?php

class Router
{
    private $routes;

    public function __construct()
    {
        $routesPath = PATH. '\config\routes.php';
        $this->routes = include($routesPath);
    }

    private function getURI()
    {
        if(!empty($_SERVER['REQUEST_URI'])){
           return trim($_SERVER['REQUEST_URI'], '/');
        }
    }

    public function run()
    {
      $uri = $this->getURI();
      foreach ($this->routes as $uriPattern => $path){

         if(preg_match("~$uriPattern~", $uri)){

             $internalRoute = preg_replace("~$uriPattern~", $path, $uri);

             $segments = explode('/', $internalRoute);

             $controllerName = array_shift($segments).'Controller'; // выбирает первый элемент массива и удаляет его из массива
             $controllerName = ucfirst($controllerName);

             $actionName = 'action'.ucfirst(array_shift($segments));

             $parameters = $segments;

             $controllerFile = PATH . '\controllers\\' . $controllerName . '.php';
             if(file_exists($controllerFile)){
                 include_once($controllerFile);
             }

             $controllerObject = new $controllerName;
             $result = call_user_func_array(array($controllerObject, $actionName), $parameters); // вызывает метод $actionName у обьекта $controllerObject передавая параметры $parameters(массив)
             //var_dump($result);
             break;
//             if($result != null){
//                 break;
//             }
         }
      }
    }
}
