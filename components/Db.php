<?php
/**
 * Created by PhpStorm.
 * User: GS
 * Date: 21.12.2018
 * Time: 14:39
 */

class Db
{
    public static function getConnection()
    {
        $paramsPath = PATH . '\config\db_params.php';
        $params = include($paramsPath);

        $dsn = "mysql:host={$params['host']};dbname={$params['dbname']}";
        $db = new PDO($dsn, $params['user'], $params['password']);

        return $db;
    }

}