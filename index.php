<?php

// общие настройки
// включение ошибок
//define('PATH', __DIR__);
define('PATH', $_SERVER['DOCUMENT_ROOT']);
ini_set('display_errors', 1);
error_reporting(E_ALL);

// Подключение файлов системы
require_once PATH .'\components\Router.php';
require_once PATH .'\components\Db.php';

//Вызов Router
$router = new Router();
$router->run();
?>