<?php
/**
 * Created by PhpStorm.
 * User: GS
 * Date: 12.02.2019
 * Time: 14:30
 */

class Catalog
{
//    Вывод всех продуктов из базы данных
//    public static function getProducts()
//    {
//        $db = DB::getConnection();
//        $itemList = array();
//
//        $result = $db->query('SELECT i.id, i.name, b.brand_name, t.type_name, s.size_name FROM items i LEFT JOIN brands b ON (i.brand_id=b.id) LEFT JOIN types t ON (i.type_id=t.id) LEFT JOIN sizes s ON (i.size_id=s.id) ORDER BY i.id DESC');
//        $result->setFetchMode(PDO::FETCH_ASSOC);
//        while ($row[] = $result->fetch()) {
//            $itemList = $row;
//        }
//        return $itemList;
//    }

   // Вывод продууктов по поиску
    public static function getProductBySearch($searchProduct)
    {
        $db = DB::getConnection();
        $productBySearch = array();
        $searchProduct = trim($searchProduct);
        $strArr = explode(" ", $searchProduct); //конвертируем строку запроса в массив
        $countStrArr = count($strArr);
        $compare = 0; //счетчик совпадений в строке бд
        $condition = array();

        $i = 1;//счетчик
        foreach ($strArr as $str){
            if($i < $countStrArr){
                $condition[] = "CONCAT(i.name, b.brand_name, t.type_name) LIKE '%".$str."%' OR ";
                $i++;
            }else{
                $condition[] = "CONCAT(i.name, b.brand_name, t.type_name) LIKE '%".$str."%' ORDER BY i.id";
            }
        }

        $result = $db->query('SELECT i.id, i.name, b.brand_name, t.type_name, s.size_name FROM items i LEFT JOIN brands b ON (i.brand_id=b.id) LEFT JOIN types t ON (i.type_id=t.id) LEFT JOIN sizes s ON (i.size_id=s.id) WHERE '. implode("", $condition));
        $result->setFetchMode(PDO::FETCH_ASSOC);
        while ($row = $result->fetch()) {
                $count = 0;
                $productName = str_replace(' ', '', $row['name']);
                $productName .= str_replace(' ', '', $row['brand_name']) . $row['type_name']; // строка из слов без пробелов состоящаяя из имени, брэнда и типа продукта
                foreach ($strArr as $word){
                    if(stripos($productName, $word) !== FALSE){
                        $count++; //проверям наличие строки запроса в созданной общей строке и производим подсчет количества совпадений
                    }
                }
//сравнение количества совпадений в каждой строке из базы данных.
                if($count > $compare){ // если совпадений в данной строке бд больше чем в предыдущей, то массив обнуляется и данная строка добавляется в него.
                    $productBySearch = array();
                    $productBySearch[] = $row;
                    $compare = $count;
                }elseif($count == $compare){ //если же в данной строке бд совпадений такое же количество, то добавляем элемент в массив.
                    $productBySearch[] = $row;
                }

        }
        return $productBySearch;
    }


//Вывод прродуктов основаный на запросе фильтрации
    public static function getProductByFilter($type, $brand = '', $size = '')
    {
        $db = Db::getConnection();
        $productsByFilter = array();
        $condition = array();
        if (!empty($brand) && !empty($size)) {
            $condition[] = " AND i.brand_id='".$brand."' AND i.size_id='".$size."'";
        } elseif (!empty($brand) || !empty($size)) {
            $condition[] = " AND (i.brand_id='".$brand."' OR i.size_id='".$size."')";
        }elseif (empty($brand) && empty($size) && !empty($type)){
            $condition[] = ' ';
        }
        $result = $db->query('SELECT i.id, i.name, i.brand_id, i.size_id, i.type_id, b.brand_name, t.type_name, s.size_name FROM items i LEFT JOIN brands b ON (i.brand_id=b.id) LEFT JOIN types t ON (i.type_id=t.id) LEFT JOIN sizes s ON (i.size_id=s.id) WHERE i.type_id="'.$type.'"'. implode("", $condition). 'ORDER BY i.id DESC LIMIT 100');
        $result->setFetchMode(PDO::FETCH_ASSOC);
        while ($row[] = $result->fetch()) {
            $productsByFilter = $row;
        }
        return $productsByFilter;
    }



    //данный вариант фильтрации имеет место быть, но как показала практика, скорость работы значительно ниже чем в примере ниже
//    public static function getProductByFilter($type, $brand = '', $size = '')
//    {
//        $db = Db::getConnection();
//        $productsByFilter = array();
//
//        if(empty($brand) && empty($size) && !empty($type)){
//    $result = $db->query("SELECT i.name, b.brand_name, t.type_name, s.size_name FROM items i LEFT JOIN brands b ON (i.brand_id=b.id) LEFT JOIN types t ON (i.type_id=t.id) LEFT JOIN sizes s ON (i.size_id=s.id) WHERE i.type_id='".$type."'");
//        }elseif(!empty($brand) || !empty($size)){
//            $result = $db->query("SELECT i.name, b.brand_name, t.type_name, s.size_name FROM items i LEFT JOIN brands b ON (i.brand_id=b.id) LEFT JOIN types t ON (i.type_id=t.id) LEFT JOIN sizes s ON (i.size_id=s.id) WHERE i.type_id='".$type."' AND i.brand_id='".$brand."' OR i.size_id='".$size."'");
//        }elseif(!empty($brand) && !empty($size)){
//    $result = $db->query("SELECT i.name, b.brand_name, t.type_name, s.size_name FROM items i LEFT JOIN brands b ON (i.brand_id=b.id) LEFT JOIN types t ON (i.type_id=t.id) LEFT JOIN sizes s ON (i.size_id=s.id) WHERE i.type_id='".$type."' AND i.brand_id='".$brand."' AND i.size_id='".$size."'");
//        }
//        $result->setFetchMode(PDO::FETCH_ASSOC);
//        $i = 0;
//        while($row[] = $result->fetch()){
//            $productsByFilter = $row;
//            $i++;
//        }
//var_dump($i);
//        return $productsByFilter;
//    }


//добавление в бд
//    public static function insertProduct(){
//        $db = Db::getConnection();
//        $arr_name = array('timberland', '1', '2', '3', 'dress', 'punch', 'boots', 'White', 'Sand', 'Saucony', 'Original', 'Black ', 'Pro', 'Shamrock', 'Bastion', 'inter', 'mark brash', 'urban planet', 'revol');
////        $arr_name = array('timberland', '1', '2', '3', 'shoes', 'punch', 'boots', 'White', 'Sand', 'Saucony', 'Original', 'Black ', 'Pro', 'Shamrock', );
//for($i = 1; $i < 25000; $i++){
//    $name = '';
//    for($y = 0; $y < 4; $y++){
//        $name .= $arr_name[rand(0, 13)] . ' ';
//    }
//
//    $result = $db->prepare('INSERT INTO items (name, brand_id, size_id, type_id) VALUES (:pname, :pbrand_id, :psize_id, :ptype_id)');
//
//    $result->execute([
//        'pname' => $name,
//        'pbrand_id' => rand(1, 9),
//        'psize_id' => rand(1, 3),
//        'ptype_id' => '1',
//    ]);
//}
//
//
//    }
}
