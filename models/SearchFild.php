<?php
/**
 * Created by PhpStorm.
 * User: GS
 * Date: 13.02.2019
 * Time: 16:21
 */

class SearchFild
{
    public static function getSizes($type)
    {
        $db = DB::getConnection();
        $sizes = array();

        $result = $db->query('SELECT * FROM sizes WHERE type_id="'.$type.'"');
        $result->setFetchMode(PDO::FETCH_ASSOC);
        while($row = $result->fetch()){
            $sizes[] = $row;
        }
        return $sizes;
    }

    public static function getBrands($type){

        $db = DB::getConnection();
        $brands = array();

        $result = $db->query('SELECT i.brand_id, b.brand_name FROM items i LEFT JOIN brands b ON (b.id = i.brand_id) WHERE i.type_id="'.$type.'" GROUP BY i.brand_id');
        while($row = $result->fetch()){
            $brands[] = $row;
        }
        return $brands;
    }



//    public static function qwerty($i)
//    {
//        $db = DB::getConnection();
//
//        $result = $db->prepare('INSERT INTO sizes (size_name, type_id) VALUES (:size, :type) ');
//        $result->execute([
//            'size' => $i,
//            'type' => 2,
//        ]);
//    }
}