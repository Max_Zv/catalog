<?php
/**
 * Created by PhpStorm.
 * User: GS
 * Date: 12.02.2019
 * Time: 15:03
 */

include_once PATH . '/models/Catalog.php';
include_once PATH . '/models/SearchFild.php';

class CatalogController
{

    public function actionIndex()
    {
        $products = array();
        if((isset($_POST['type_product']) || isset($_POST['size_product']) || isset($_POST['brand_product'])) && !empty($_POST['type_product'])){
            $type_product = $_POST['type_product'];
            $size_product = $_POST['size_product'];
            $brand_product = $_POST['brand_product'];
            $products = Catalog::getProductByFilter($type_product, $brand_product, $size_product);
        }elseif(isset($_POST['search'])){
                $search = $_POST['search'];
                $products = Catalog::getProductBySearch($search);

        }

        require_once (PATH . '/views/catalog/index.php');
        return true;
    }


    //Обработчик Ajax запроса формы выпадающего меню
    public function actionSearchFild()
    {
        $sizes = array();
        $sizes = SearchFild::getSizes($_POST['type']);

        $brands = array();
        $brands = SearchFild::getBrands($_POST['type']);

        //Подготовка массивов для конвертирования в JSON
        foreach ($sizes as $size){
            $prepareSizeToJson[$size['id']] = $size['size_name'];
        }

        foreach ($brands as $brand) {
            $prepareBrandToJson[$brand['brand_id']] = $brand['brand_name'];
        }

        //Конвертирование массива в JSON
        $resultToJson = array('size' => $prepareSizeToJson, 'brand' => $prepareBrandToJson);

        echo json_encode($resultToJson);
        return true;
    }

}




