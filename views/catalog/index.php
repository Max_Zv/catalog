<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="/templates/css/main.css" rel="stylesheet">
    <link href="/templates/css/responsive.css" rel="stylesheet">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <title>Document</title>
</head>
<body>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<div class="container">
    <div class="row">
        <div class="col-sm-3">
        <form method="post">
            <input type="text" name="search" placeholder="поиск" /><br/>
            <input type="submit" value="Поиск">
        </form>
        <form method="post">
            <select name="type_product">
                <option value="">Тип одежды</option>
                <option value="1">Платья</option>
                <option value="2">Обувь</option>
            </select><br/>
            <select name="brand_product">
                <option value=" "></option>
            </select><br/>
            <select name="size_product">
                <option value=" "></option>
            </select>
            <input type="submit" value="Поиск">
        </form>
        <script type="text/javascript">
            $(document).ready(function() {
                $('select[name="type_product"]').bind('change', function(){
                    $('select[name="size_product"]').empty();
                    $('select[name="brand_product"]').empty();
                    var type = $('select[name="type_product"]').val();
                    $.ajax({
                        url: 'searchfild',
                        type: 'post',
                        data: {type: type} ,
                        dataType: 'json',
                        success: function(data){
                            for(var type in data){
                                if(type=='size'){
                                    $('select[name="size_product"]').append($('<option value="">Выберите размер</option>'));
                                    for(var id in data[type]){
                                        $('select[name="size_product"]')
                                            .append($('<option value="'+ id +'">'+ data[type][id] +'</option>'));
                                    }
                                }else{
                                    $('select[name="brand_product"]').append($('<option value="">Выберите брэнд</option>'));
                                    for(var id in data[type]){
                                        $('select[name="brand_product"]')
                                            .append($('<option value="'+ id +'">'+ data[type][id] +'</option>'));
                                    }
                                }
                            }
                        }
                    });
                });
            });
        </script>
        </div>
        <div class="col-sm-9 padding-right">
            <div class="row">
                    <?php foreach ($products as $product):?>
                        <div class="col-sm-4">
                            <div class="product-image-wrapper">
                                <div class="single-products">
                                    <div class="productinfo text-center">
                                        <h2><?php echo $product['brand_name'];?></h2>
                                            <?php echo $product['name']?><br/>
                                            <?php echo $product['type_name']?><br/>
                                            <?php echo $product['size_name']?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php endforeach;?>

            </div>
        </div>
    </div>
</div>
</body>
</html>